package com.qa.testcases;

import java.io.IOException;

import org.apache.commons.exec.LogOutputStream;
import org.testng.Reporter;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.qa.base.TestBase;
import com.qa.pageActions.EOITestHarnessPageAction;
import com.qa.pageActions.HomePageAction;
import com.qa.pageActions.LoginPageAction;

import com.qa.util.GenericFunction;
import com.qa.util.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

public class TestHarnessSetUpTest extends TestBase{
	public TestHarnessSetUpTest() {
		super();	
	}
	
/* *****************************************************************************
	  * Test Name : Test Harness setup instructions automation
	  * Purpose : To validate the set instructions of test harness
	  * History : Created by Anjali Johny on 07/29/2021 	 
 **************************************************************************************/
LoginPageAction loginPageAction;
HomePageAction homePageAction;
EOITestHarnessPageAction eoiTestHarnessPageAction;
GenericFunction genericFunction = new GenericFunction();
@DataProvider
public Object[][] getTestHarnesstatus(){
	Object data[][] = TestUtil.getTestData("TestHarness");
	return data;
}


@Test(priority=1,dataProvider="getTestHarnesstatus")
    public void TestHarnesssSetUp(String userId, String passWord,String uniqueNameId,String eoixml,String Employer,String Groupno) throws Exception {
	Thread.sleep(3000);
	extentTest = extent.startTest("Verifying EOI Test Harness SetUp Process");
	intialization();
	extentTest.log(LogStatus.INFO, "URL: " + prop.getProperty("url"));
	loginPageAction = new LoginPageAction();
	homePageAction= loginPageAction.userLogin(userId, passWord);//User will login using this function
	eoiTestHarnessPageAction=homePageAction.validateHomepage(prop.getProperty("jsonUrl"));
	homePageAction=eoiTestHarnessPageAction.validateJspPage(uniqueNameId,prop.getProperty("jsonUrl"),eoixml);
	//homePageAction.logOut();
	homePageAction.validateConfirmHomePage(Employer,Groupno);

	
}}
