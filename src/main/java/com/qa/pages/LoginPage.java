package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class LoginPage extends GenericFunction{
	/* ***********************************************************************************
	Class Name  : LoginPage
	Purpose     : This class contains all objects of Login page
	History     : 07/02/2021 by Anjali Johny
	**************************************************************************************/
	@FindBy(id="username2")
	public WebElement txtUserName;
	
	@FindBy(id="password2")
	public WebElement txtPassword;
	
	@FindBy(id="oktaSignIn2")
	public WebElement btnLogin;
	
	public LoginPage() {
		super();
		PageFactory.initElements(driver, this);
	}
	
}