package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class HomePage extends GenericFunction{
	
	/* ***********************************************************************************
	Class Name  : HomePage
	Purpose     : This class contains all objects of Home page
	History     : 07/05/2021 by Anjali Johny
	**************************************************************************************/
	
	@FindBy(xpath="//div[@class='main-content-inner']//a[contains(text(),'Home')]")
	public WebElement linkHomepage;
	@FindBy(xpath="//div[@class='landing-page visible-lg']//h1[contains(text(),'Welcome to OneAmerica')]")
	public WebElement headerHomePage;
	@FindBy(xpath="//div[@class='landing-page visible-lg']//button[contains(text(),'Begin EOI')]")
	public WebElement btnBeginEOI;
	@FindBy(xpath="//div[@id='body']//span[contains(text(),'Pre-Approved')]")
	public WebElement lblPreApprovedStatus;
	@FindBy(xpath="//div[@id='body']//span[contains(text(),'Referred')]")
	public WebElement lblReferredStatus;
	@FindBy(xpath="//div[@id='sidebar']//li//a//i[@class='menu-icon fa fa-sign-out']")
	public WebElement linkLogOut;
	@FindBy(xpath="//div[@class='landing-page visible-lg']//button[contains(text(),'View Confirmation')]")
	public WebElement lblViewConfirmation;
	@FindBy(xpath="//div[@class='landing-page visible-lg']//button[ contains(text(),'Add Contact Info')]")
	public WebElement btnAddContactInfo;
	@FindBy(xpath="//table[@class='group-info-tablet-landscape']//tr[2]//td[1]")
	public WebElement txtEmployer;
	@FindBy(xpath="//table[@class='group-info-tablet-landscape']//tr[2]//td[2]")
	public WebElement txtEmployerName;
	@FindBy(xpath="//table[@class='group-info-tablet-landscape']//tr[3]//td[1]")
	public WebElement txtGroupNumber;
	@FindBy(xpath="//table[@class='group-info-tablet-landscape']//tr[3]//td[2]")
	public WebElement txtGroupNumbeRValue;
	
	public HomePage() {
		super();
		PageFactory.initElements(driver, this);
	}
}