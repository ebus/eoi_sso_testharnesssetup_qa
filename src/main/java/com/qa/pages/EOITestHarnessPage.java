package com.qa.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.qa.util.GenericFunction;

public class EOITestHarnessPage extends GenericFunction{
	
	/* ***********************************************************************************
	Class Name  : HomePage
	Purpose     : This class contains all objects of Home page
	History     : 07/05/2021 by Anjali Johny
	**************************************************************************************/
	
	@FindBy(xpath="//h4[contains(text(),'Generate SAML')]")
	public WebElement headerGenerateSAML;
	@FindBy(id="nameId")
    public WebElement txtNameId;
	@FindBy(xpath="//button[contains(text(),'Generate SAML')]")
	public WebElement btnGenerateSaml;
	@FindBy(xpath="//h4[contains(text(),'Login')]")
	public WebElement headerLogin;
	@FindBy(id="SAMLResponse")
	public WebElement txtareaSamlResponse;
	@FindBy(xpath="//textarea[contains(text(),'Will fill in with contents after clicking Generate SAML...')]")
	public WebElement txtSamlDefaultResponse;
	@FindBy(xpath="//form[@id='frmSaml']//input[@value='Login']")
	public WebElement btnLogin;
	@FindBy(xpath="//textarea[@id='eoi']")
	public WebElement txtareaEOIxml;
	public EOITestHarnessPage() {
		super();
		PageFactory.initElements(driver, this);
	}
}