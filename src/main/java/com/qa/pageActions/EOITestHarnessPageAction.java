package com.qa.pageActions;

//import static org.junit.Assert.assertTrue;
import static org.testng.Assert.assertFalse;

import com.qa.pages.EOITestHarnessPage;
import com.relevantcodes.extentreports.LogStatus;


public class EOITestHarnessPageAction extends EOITestHarnessPage {
	public EOITestHarnessPageAction() {
		super();
	}
	/* *****************************************************************************
	  * Test Name :EOITestHarnessPageAction 
	  * Purpose : This class contains the action of TestHarness page
	  * History : Created by Anjali Johny on 07/05/2021 
   ***********************************************************************************/
	public HomePageAction validateJspPage(String NameId,String jsonurl,String eoixml) throws InterruptedException {
		extentTest.log(LogStatus.PASS, "New tab is opened and url: "+jsonurl+" is entered");
	    assertText(headerGenerateSAML,"Generate SAML","Generate SAML header");
	    //txtNameId.clear();
		EnterText(txtNameId,NameId,"NameID field");
		assertElementDisplayed(txtSamlDefaultResponse,"SAML Default Response text");
		takeScreenshot("TestHarnessPage_BeforeGenarateSAML");
		try
	 	{
	 		
	 		if(!eoixml.isEmpty())
	 		{
	 			eoixml = eoixml.trim();
	 			txtareaEOIxml.clear();			
	 			txtareaEOIxml.sendKeys(eoixml);
	 			extentTest.log(LogStatus.PASS,"EOI XML is entered" );
	 		}
	 		else
	 		{
	 			txtareaEOIxml.sendKeys(eoixml);
	 			extentTest.log(LogStatus.PASS,"EOI XML is entered");
	 			//extentTest.log(LogStatus.FAIL, extentTest.addScreenCapture(failedScreenshotForReport()));
	 		}	
	 	}	
	 	catch(Exception e)
	 	{
	 		e.printStackTrace();
	 		extentTest.log(LogStatus.FAIL,"EOI XML is not entered to " +eoixml);
	 		extentTest.log(LogStatus.FAIL, extentTest.addScreenCapture(failedScreenshotForReport()));
	 	}
		
		Thread.sleep(1000);
		ClickElement(btnGenerateSaml,"Generate SAML Button");
		Thread.sleep(2000);
		takeScreenshot("TestHarnessPage_AfterGenarateSAML");
		assertElementDisplayed(headerLogin,"Login header");
		scrollIntoView(btnLogin, driver);
		takeScreenshot("TestHarnessPage_Login");
		ClickElement(btnLogin, "Login Button");
		Thread.sleep(5000);
		
		return new HomePageAction();
		
	}

	

	}