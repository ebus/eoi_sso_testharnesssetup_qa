package com.qa.pageActions;

import com.qa.pages.LoginPage;
import com.relevantcodes.extentreports.LogStatus;

public class LoginPageAction extends LoginPage {
	public LoginPageAction() {
		super();
	}
	/* *****************************************************************************
	  * Test Name : LoginPageAction 
	  * Purpose : This class contains the action of  the login page
	  * History : Created by Anjali Johny on 07/02/2021 
	  **************************************************************************************/
	public HomePageAction userLogin(String userId, String passWord) throws InterruptedException {
		extentTest.log(LogStatus.INFO, "------- LoginPage -------");
		EnterText(txtUserName, userId, "UserID");		
		EnterPassword(txtPassword, passWord, "PassWord");
		takeScreenshot("LoginPage");
		ClickElement(btnLogin, "Login");
		Thread.sleep(3000);
		return new HomePageAction();		
	}

}
//UpdateSecurityInformationPageAction

//HomePageAction