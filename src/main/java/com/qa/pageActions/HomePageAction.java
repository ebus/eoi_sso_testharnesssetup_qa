package com.qa.pageActions;

import static org.testng.Assert.assertTrue;

import java.util.ArrayList;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;

import com.qa.pages.HomePage;
import com.qa.pages.LoginPage;
import com.relevantcodes.extentreports.LogStatus;

public class HomePageAction extends HomePage {
	public HomePageAction() {
		super();
	}
	/* *****************************************************************************
	  * Test Name :HomePageAction 
	  * Purpose : This class contains the action of the Home page
	  * History : Created by Anjali Johny on 07/05/2021 
	  * PreRequisite: EmploymentKey value in the XML should have “EOI Test Harness” as the vendor  and 'SSO'/'Both' 
	  *               selected for EOI Sign-on Type in the Employee Benefits website(eBen) EOI Policyholder Registration menu item.
	
   ***********************************************************************************/
	public EOITestHarnessPageAction validateHomepage(String jsonUrl) throws InterruptedException {
	    Thread.sleep(2000);
		extentTest.log(LogStatus.PASS, "Home page is displayed");
		assertElementDisplayed(linkHomepage,"Home page link");
		assertElementDisplayed(headerHomePage,"Welcome to OneAmerica");
		takeScreenshot("HomePageUp_BeforeSetUp");
		Thread.sleep(1000);
		scrollPageDown(driver);
		takeScreenshot("HomePageDown_BeforeSetUp");
		Actions action = new Actions(driver);
	    ((JavascriptExecutor)driver).executeScript("window.open()");
	    ArrayList<String> tabs = new ArrayList<String>(driver.getWindowHandles());
		driver.switchTo().window(tabs.get(1));
		driver.get(jsonUrl);
		takeScreenshot("TestHarness_jsp_Page");
		Thread.sleep(4000);
		return new EOITestHarnessPageAction();
	
	}
	public void validateConfirmHomePage(String employer,String groupno) {
		 
			extentTest.log(LogStatus.PASS, "Home page is displayed");
			assertElementDisplayed(linkHomepage,"Home page link");
			assertElementDisplayed(headerHomePage,"Welcome to OneAmerica");
			takeScreenshot("HomePageUp_AfterSetUp");
			scrollIntoView(headerHomePage, driver);
			assertText(txtEmployer,"Employer:","Employer:");
			assertText(txtEmployerName, employer,"Employer");
			assertText(txtGroupNumber,"Group Number:","Group Number:");
			assertText(txtGroupNumbeRValue, groupno,"Group Number");
			takeScreenshot("HomePage_GroupAndEmployer_Detail");
			scrollIntoView(txtEmployer, driver);
			takeScreenshot("HomePageDown_AfterSetUp");
			
	}
	
	
	}